# Afficher l'historique des commits (git log)

# git log

Rémy Huet, Thibaud Duhautbout, Quentin Duchemin, Roma Maliach, Stéphane Crozat

Le `git log` permet de voir l'historique de tous les commits effectués sur un `repository`.

```sh
git log
```

![](git-restore.png)

On peut voir ici :

- L'identifiant unique des commits ;
- Les auteurs ;
- Les dates des commit ;
- Les messages qui ont été entrés lors des commits.

Le `git diff` permet de voir les modifications apportées au `working directory` :

- depuis l'état du `staging area` : `git diff`
- depuis le dernier commit : `git diff HEAD`
- depuis un commit quelconque : `git diff id_commit`

![](git-diff-2.png)

On peut observer sur cet exemple que entre le `working directory` et le dernier commit :

- le `myfile.txt` a été modifié,
- la ligne `Ceci n'est pas une phrase.` a été supprimée de ce fichier,
- la ligne `Ceci est-il une phrase ?` a été ajoutée à ce fichier.

Les instruction `git log` et `git diff` peuvent être suivies du nom d'un fichier afin de n'afficher l'historique ou les différences qui concernent ce fichier.

![](git-diff.png)

On peut observer sur cet exemple qu'entre `working directory` et le commit `30676ede3ab1aaedc3cd8a6f8abc749efa552858`, à propos du fichier README.md, la ligne `stephane.crozat@utc.fr` a été ajoutée.

