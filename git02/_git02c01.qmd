# Créer un dépôt distant

Rémy Huet, Thibaud Duhautbout, Quentin Duchemin, Roma Maliach, Stéphane Crozat

# Remote

Une `remote repository` (ou dépôt distant) est un dépôt Git en ligne.

Il se comporte comme un `repository` local, il stocke donc un ensemble de commits.

# Intérêt des remotes

- Partage du dépôt git : on peut désormais travailler à plusieurs dessus
- Sauvegarde du travail à distance

# Forge

Une forge est un système de gestion de versions associé à des fonctions collaboratives (discussions, gestions de bugs...).

Une forge Git est une forme qui repose sur Git comme système de gestion de version. Les forges Git sont aujourd'hui parmi les plus répandues.

# Github

Github est une énorme forge Git centralisée propriété de Microsoft depuis 2018.

[github.com](https://github.com)

# Gitlab

Gitlab est un logiciel permettant d'installer sa propre forge Git dans une logique décentralisée.

Framasoft et l'UTC hébergent chacune une instance de la forge Gitlab (il en existe beaucoup d'autres).

[gitlab.utc.fr](https://gitlab.utc.fr)

[framagit.org](https://framagit.org)

# Création d'un projet (dépôt distant)

Pour le Gitlab de l'UTC :

- Se rendre sur https://gitlab.utc.fr
- Une fois connecté avec ses identifiants CAS, il suffit d'appuyer sur le bouton « New project » et de renseigner un nom et un niveau de visibilité

# Ajout de clé SSH

Pour simplifier l'authentification au serveur, on peut ajouter sa clé publique SSH à son profil.

