# Défi : Veille

Stéphane Crozat

Créez un dépôt distant (projet) sur le Gitlab de l'UTC que vous intitulerez Veille :

- créez un fichier README.md avec votre nom (ou un pseudo),
- associez une licence (compatible avec celle de Wikipédia),
- clonez le projet en local.

Ajoutez un fichier `prism.md` qui contient le premier paragraphe concernant ce programme de la NSA sur Wikipédia.

Commitez et poussez ce fichier.

[fr.wikipedia.org/wiki/PRISM](https://fr.wikipedia.org/wiki/PRISM_(programme_de_surveillance))

Modifiez votre fichier afin d'ajouter la fin de l'introduction présente sur Wikipédia, commitez et poussez.

Supprimez votre dépôt local puis créez à nouveau le dépôt.

Accordez-vous avec un autre utilisateur que nous appellerons Édouard dans le cadre de cet exercice.

Autorisez Édouard à pousser sur votre dépôt (et demandez-lui de faire de même pour vous).

Clonez le dépôt d'Édouard et poussez un fichier `chelsea.md` sur son dépôt qui contient le premier paragraphe de présentation de Chelsea Manning sur Wikipédia.

[fr.wikipedia.org/wiki/Chelsea_Manning](https://fr.wikipedia.org/wiki/Chelsea_Manning)

Commentez le premier commit d'Édouard afin de lui conseiller d'ajouter un email à son README.

Ajoutez une issue à votre projet afin de prévoir l'ajout d'un document sur Aaron Swartz.

[fr.wikipedia.org/wiki/Aaron_Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz)

Ajoutez une issue au projet d'Édouard afin de lui demander l'ajout d'un document sur Wikileaks.

[fr.wikipedia.org/wiki/WikiLeaks](https://fr.wikipedia.org/wiki/WikiLeaks)

