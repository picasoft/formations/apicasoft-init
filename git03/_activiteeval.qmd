# La gestion des branches avec git

Que fait la commande

```sh
 git checkout -b ma_branche
```

Elle crée une branche ma_branche si ma_branche n'existe pas

Elle écrase la branche ma_branche avec une nouvelle si ma_branche existe

Elle me déplace vers la branche ma_branche si celle-ci n'existait pas avant

Elle me déplace vers la branche ma_branche si celle-ci existait déjà avant

Git checkotu -b créera une branche ma_branche et vous y déplacera. Si elle détecte que la branche existe déjà, elle renvoie une erreur et ne fait rien.

Quelles phrases sont vraies

La commande "git branch -D ma_branche" crée une branche nommée ma_branche

Cette commande SUPPRIME la branche ma_branche

La commande "git merge alice" fusionne la branche courante avec la branche alice

La commande "git rebase" est une commande dangereuse

Si l'on doit résoudre des conflits, c'est parce que l'on a mal géré ses branches

Il est tout à fait normal de devoir gérer des conflits quand certaines personnes doivent travailler dans la même partie du code.

La gestion arborescente est une spécificité de git

Pouvoir gérer des versions en arborescence est une fonctionnalité de base de beaucoup de gestionnaires de version.

C'est une bonne idée de rebase si...

Je n'ai pas produit de nouvelles versions depuis longtemps et les autres ont beaucoup avancé

J'ai effectué des modifications majeures et je dois les incorporer à un projet avant de push

J'ai effectué des modifications mineures et je dois les incorporer à un projet avant de push

La branche master a vocation à :

Contenir à terme une très large partie des commits

Être fonctionnelle à tout moment

Contenir la version la plus avancée du projet, qu'elle soit stable ou non

servir de base pour les versions publiées d'un projet

C'est souvent à partir de la branche master que l'on crée des versions. Si certaines versions nécessitent des correctifs spécifiques, on a pour coutume de créer des branches spécifiques pour appliquer des correctifs à des versions spécifiques.

