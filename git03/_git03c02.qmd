# Créer des branches et les utiliser

# git branch, git checkout

Baptiste Wojtkowski

# Créer une branche

La manière commune de créer une branche est d'utiliser

```sh
git branch ma_branche
```

Il existe d'autres manières de créer une branche, comme par exemple

```sh
git checkout -b ma_branche master
```

Cette commande dit :

1. Déplace toi sur la branche ma_branche.
2. l'option -b indique que la branche ma_branche n'existe pas et qu'il faut la créer.
3. master est le point de départ de la branche.

# Faire un commit sur une branche

Soit une branche ma_branche ayant pour dernier commit mon_commit, faire un nouveau commit sur la branche ma_branche signifie

- Créer un commit fils au commit mon_commit
- Déplacer l'étiquette de la branche sur le nouveau commit

# Vérifier la branche sur laquelle on est

Pour vérifier la branche sur laquelle on est on peut utiliser la commande git branch sans utiliser d'argument. Elle liste toutes les branches disponibles et met une étoile sur la branche master

# Ne plus avoir à vérifier la branche sur laquelle on est

Il existe de nombreuses manières de changer son prompt pour qu'il affiche en permanence l'état du dépôt sur lequel on est. Et notamment la branche sur laquelle on est.

