# La fusion de branches

# git merge

Baptiste Wojtkowski

# Fusion de branches

Fusionner des branches, c'est réunir le travail de deux personnes différentes en une seule, pour le faire, on utilise la commande git merge, celle-ci crée en général un commit de fusion

Le commit de fusion est le seul type de commit qui puisse avoir deux parents.

```sh
git merge target_branch
```

La commande précédente met toutes les modifications de la branche cible dans la branche courante.

