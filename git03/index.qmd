# Introduction à la gestion des branches avec Git

Baptiste Wojtkowski

{{< include _git03c01.qmd >}}

{{< include _git03c02.qmd >}}

{{< include _git03ex01.qmd >}}

{{< include _git03c03.qmd >}}

{{< include _git03ex02.qmd >}}

{{< include _git03c04.qmd >}}

{{< include _git03ex03.qmd >}}

