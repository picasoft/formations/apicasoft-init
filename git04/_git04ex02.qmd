# Usage des reflogs

Baptiste Wojtkowski

Reprenez votre dépôt et supprimez tous vos commits à l'exception du premier. Puis utilisez les logs de référence pour retrouver votre code.

Pour supprimer tous les commits :

```sh
git reset --hard sha_du_premier_commit
```

Comme d'habitude, vous n'aurez pas les mêmes SHA

```sh
git reflog
```

On voit ainsi la liste des opérations effectuées. On peut remarquer que le rebase -i a effectué beaucoup d'opérations atomiques (eti l est possible de revenir dessus mais on ne le fera pas dans le cadre de ce cours)

```sh
bbb7664 (HEAD -> master) HEAD@{0}: reset: moving to bbb7664
98b3866 HEAD@{1}: rebase -i (finish): returning to refs/heads/master
98b3866 HEAD@{2}: commit (amend): Création du fichier c.txt pour fonctionnalité c à implémenter
7076764 HEAD@{3}: rebase -i (edit): ajout c.txt
ea4cd9a HEAD@{4}: rebase -i (fixup): Ajout fonctionnalité b dans nouveau fichier b.txt
7d6b274 HEAD@{5}: rebase -i (squash): # Ceci est la combinaison de 2 commits.
acf3a13 HEAD@{6}: rebase -i (pick): ajout b.txt
3100695 HEAD@{7}: rebase -i (fixup): Ajout fonctionnalité a dans nouveau fichier a.txt
bdfe4cf HEAD@{8}: rebase -i (squash): # Ceci est la combinaison de 2 commits.
1ca7d13 HEAD@{9}: rebase -i : avance rapide
bbb7664 (HEAD -> master) HEAD@{10}: rebase -i : avance rapide
fa6f034 HEAD@{11}: rebase -i (start): checkout fa6f03418f4e94588824829be0afa26d1fdd29a0
ab22896 HEAD@{12}: rebase -i (abort): updating HEAD
b25c4a5 HEAD@{13}: rebase -i (start): checkout b25c4a54c1f9849e4ef87d9c70071d2085ab9dc2
ab22896 HEAD@{14}: commit: débug fonctionnalité b dans b.txt
6ccc4d7 HEAD@{15}: commit: débug fonctionnalité a dans a.txt
771f2c8 HEAD@{16}: commit: Ajout fonctionnalité b dans b.txt
1c2ab5c HEAD@{17}: commit: Ajout fonctionnalité a dans a.txt
e62f296 HEAD@{18}: commit: ajout d.txt
845ae93 HEAD@{19}: commit: ajout c.txt
e7d2ee2 HEAD@{20}: commit: ajout b.txt
1ca7d13 HEAD@{21}: commit: ajout a.txt
bbb7664 (HEAD -> master) HEAD@{22}: commit (initial): Commit initial
```

Il ne nous reste plus qu'à annuler la dernière modifications en choisissant le SHA de la version qui la précède et constater le résultat

```sh
git reset --hard 98b3866
git log --oneline
git reflog
```

On constate toutefois que reflog n'a pas réellement supprimé de l'historique mais a simplement ajouté une ligne annulant toutes les modifications effectuées depuis le commit sélectionné.

