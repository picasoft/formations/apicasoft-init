# Contexte

Stéphane Crozat, Marc Damie

*Durée* : 2h

*Environnement de travail* : Linux en ligne de commande

*Pré-requis* : Aucun

![](21992_ODCco_22004.net)

Le chiffrement est une technique qui permet de garantir la confidentialité et l'origine des informations électroniques échangées ou stockées.

Pour garantir la confidentialité d'une information on applique un code qui rend le message incompréhensible tant que l'on ne connaît pas la manière de décoder. On appelle clé l'information qui permet de coder et de décoder.

Le chiffrement est aussi une façon de signer des informations afin de garantir qu'on en est l'émetteur.

L'usage du chiffrement s'est fortement développé ces dernières années notamment sur le Web avec la diffusion du protocole HTTPS. La multiplication des fuites de données et le développement de la cybercriminalité en sont une première cause.

Les révélations d'Edward Snowden en 2013 ont également montré que les états et les grandes entreprises espionnaient les échanges sur Internet. Le chiffrement est donc également un outil pour préserver l'intimité de chacun en ligne.

