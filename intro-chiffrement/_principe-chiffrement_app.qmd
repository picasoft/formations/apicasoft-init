# Appliquer la notion

Stéphane Crozat, Marc Damie

Décrypter le message suivant, chiffré par décalage  :

```
nc ocejkpg gpkioc pgvckv rcu ugewtkugg
```

On ne connaît pas la clé, mais on sait que le premier mot est *la*.

On peut calculer le décalage à partir de notre connaissance du premier mot : il correspond à la différence de position entre les lettres *n* et *l*, ou *c* et *a*.

Le décalage était 2 et le message : « `La machine Enigma n'était pas sécurisée` »

