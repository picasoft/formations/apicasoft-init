# Afficher des fichiers

Kyâne Pichou

# Objectifs

- Savoir afficher tout ou partie des fichiers dans la console

# Afficher des fichiers

Nous savons comment ouvrir des fichiers avec Nano pour les éditer directement dans la console. On pourrait donc se demander l'intérêt d'utiliser d'autres commandes pour afficher le contenu d'un fichier dans la console, puisque l'on peut le consulter avec Nano. Cependant cet outil reste un éditeur de texte, il est prévu pour éditer des fichiers et possède une interface qui lui est propre. Avoir une commande pour afficher le contenu d'un fichier *et c'est tout* permet de facilement récupérer le contenu d'un fichier pour le manipuler (même si l'on ne sait pas encore comment faire des scripts).

# La commande cat

On peut utiliser `cat` (abréviation de `concatenate`) pour afficher le contenu d'un fichier dans le terminal.

```sh
kyane@europa:~/librecours$ cat roman.txt 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ornare vitae sem sed cursus. Maecenas sed mi eget ligula ullamcorper dapibus. 
Nullam eu elit non dui maximus vulputate. Ut dolor est, commodo eu finibus nec, imperdiet et lacus. Morbi at mauris eros. Nullam pharetra aliquet mauris id finibus. 

```

Comme son nom l'indique, la commande peut servir à afficher (et donc concaténer) plusieurs fichiers.

```sh
kyane@europa:~/librecours$ cat roman.txt suite.txt 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ornare vitae sem sed cursus. Maecenas sed mi eget ligula ullamcorper dapibus. 
Nullam eu elit non dui maximus vulputate. Ut dolor est, commodo eu finibus nec, imperdiet et lacus. Morbi at mauris eros. Nullam pharetra aliquet mauris id finibus. 
Pellentesque lectus dui, iaculis ac lorem in, vehicula porttitor urna. Mauris tincidunt, neque et rutrum placerat, leo dolor tempus felis, ac accumsan odio lacus nec nisl. 
Nullam vulputate mattis aliquam. Vestibulum cursus dictum metus. 
In iaculis orci eget mauris maximus, non mollis augue lacinia. Nulla viverra lorem lectus, vel cursus odio mollis quis. Aenean iaculis dui pellentesque, gravida turpis at, porta nibh. 

```

On se rend compte cependant que `cat` est adapté lorsque l'on veut visualiser de petits fichiers. Essayons avec le fichier `/var/log/syslog` qui contient des messages d'information du système d'exploitation.

```sh
cat /var/log/syslog
```

Énormément de texte s'affiche dans notre console, ce n'est pas pratique à lire puisque qu'il faut remonter tout en haut du fichier.

# La commande less

Pour nous aider à visualiser le fichiers qui sont très grand, on utilise la commande `less`. Cet outil ouvre une interface très basique qui permet de visualiser le fichier et de se déplacer dedans.

![](less.png)

On peut utiliser quelques commandes :

- parcourir le fichier à l'aide des flèches `Haut/Bas`, mais aussi faire de plus grand sauts avec les flèches `Page Up/Page Down` ;
- quitter l'interface avec la touche `q`;
- `g` permet de se rendre au début du fichier ;
- `G` permet de se rendre en fin de fichier ;
- écrire `/` suivi d'un mot permet de rechercher le mot dans le document.

# Récupérer des portions de fichier

Afficher un fichier dans sa totalité c'est bien, mais dans certains cas on ne souhaite pas forcément récupérer tout le fichier. Par exemple pour notre fichier de log `/var/log/syslog`, on pourrait ne vouloir consulter que les 20 dernières lignes, pour voir les 20 événements les plus récents (chaque message supplémentaire de l'OS est inscrit à la fin du fichier). On peut utiliser `cat` et ne regarder que la fin, mais ce n'est pas très pratique si le fichier est très gros `cat` mettra du temps à l'afficher, et si l'on ne veut consulter que les premières lignes à l'inverse on est toujours embêté.

# tail

La commande `tail` (qui signifie `queue` en anglais), permet justement de n'afficher que les dernières lignes d'un fichier.

```sh
kyane@europa:~/librecours$ tail /var/log/syslog
Nov 27 14:40:42 europa org.freedesktop.thumbnails.Thumbnailer1[20118]: Registered thumbnailer evince-thumbnailer -s %s %u %o
Nov 27 14:40:42 europa org.freedesktop.thumbnails.Thumbnailer1[20118]: Registered thumbnailer dia -t png -e %o -s %s %i
Nov 27 14:40:42 europa org.freedesktop.thumbnails.Thumbnailer1[20118]: Registered thumbnailer /usr/bin/gdk-pixbuf-thumbnailer -s %s %u %o
Nov 27 14:40:42 europa org.freedesktop.thumbnails.Thumbnailer1[20118]: Registered thumbnailer atril-thumbnailer -s %s %u %o
Nov 27 14:40:42 europa org.freedesktop.thumbnails.Thumbnailer1[20118]: Registered thumbnailer /usr/bin/gdk-pixbuf-thumbnailer -s %s %u %o
Nov 27 14:40:43 europa dbus-daemon[1976]: [session uid=1000 pid=1976] Successfully activated service 'org.freedesktop.thumbnails.Cache1'
Nov 27 14:40:43 europa dbus-daemon[1976]: [session uid=1000 pid=1976] Successfully activated service 'org.freedesktop.thumbnails.Thumbnailer1'
Nov 27 14:40:43 europa tumblerd[20118]: Name org.freedesktop.thumbnails.Cache1 lost on the message dbus, exiting.
Nov 27 14:40:43 europa tumblerd[20118]: Name org.freedesktop.thumbnails.Manager1 lost on the message dbus, exiting.
Nov 27 14:40:43 europa tumblerd[20118]: Name org.freedesktop.thumbnails.Thumbnailer1 lost on the message dbus, exiting.

```

Par défaut la commande ne retourne que les 10 dernières lignes, mais on peut utiliser le paramètre `-n` pour en obtenir un nombre précis.

```sh
kyane@europa:~/librecours$ tail -n 5 /var/log/syslog
Nov 27 14:40:43 europa dbus-daemon[1976]: [session uid=1000 pid=1976] Successfully activated service 'org.freedesktop.thumbnails.Cache1'
Nov 27 14:40:43 europa dbus-daemon[1976]: [session uid=1000 pid=1976] Successfully activated service 'org.freedesktop.thumbnails.Thumbnailer1'
Nov 27 14:40:43 europa tumblerd[20118]: Name org.freedesktop.thumbnails.Cache1 lost on the message dbus, exiting.
Nov 27 14:40:43 europa tumblerd[20118]: Name org.freedesktop.thumbnails.Manager1 lost on the message dbus, exiting.
Nov 27 14:40:43 europa tumblerd[20118]: Name org.freedesktop.thumbnails.Thumbnailer1 lost on the message dbus, exiting.

```

# head

L'opposée de `tail` est la commande `head` (qui signifie `tête`) qui peut-être utilisée de la même manière, mais pour afficher le début d'un fichier.

```sh
kyane@europa:~/librecours$ head -n 5 /var/log/syslog
Nov 27 08:14:53 europa blueman-mechanism[701]: Unable to init server: Could not connect: Connection refused
Nov 27 08:14:53 europa systemd[1]: logrotate.service: Succeeded.
Nov 27 08:14:53 europa systemd[1]: Finished Rotate log files.
Nov 27 08:14:53 europa containerd[858]: time="2020-11-27T08:14:53.758802087+01:00" level=info msg="loading plugin \"io.containerd.content.v1.content\"..." type=io.containerd.content.v1
Nov 27 08:14:53 europa systemd[1]: Started User Login Management.

```

# À retenir

Pour afficher des fichiers dans la console, on utilise des commandes dédiées plutôt que d'ouvrir le fichier avec Nano. On utilise `cat` plutôt pour les petits fichiers, et `less` pour les plus grands. Pour récupérer uniquement le début ou la fin d'un fichier, on s'appuie sur les commandes `head` et `tail`.

