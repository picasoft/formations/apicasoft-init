# Contexte

Stéphane Bonnet, Antoine Lima, Kyâne Pichou

*Durée* : 2h

*Environnement de travail* : Terminal Linux

*Pré-requis* : Aucun

Lorsque l'on utilise un ordinateur, on manipule souvent des fichiers. Linux ne fait pas exception, et nous allons voir qu'il existe de nombreux outils en ligne de commande qui permettent de créer, consulter, modifier, et organiser les fichiers et dossiers de l'ordinateur. Nous allons d'abord voir comment ils sont gérés et organisés, puis nous verrons les outils pour les manipuler.

