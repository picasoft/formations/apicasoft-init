# Appliquer la notion

Kyâne Pichou

Créez un dossier vide dans lequel réaliser l'exercice. Une fois placé dans ce dossier, copiez/collez les commandes suivantes pour préparer "l'environnement".

```sh
mkdir -p imgaes/vacances-2019 imgaes/noel-2018 imgaes/a-developper
touch imgaes/vacances-2019/photo1.jpg imgaes/vacances-2019/photo2.jpg imgaes/vacances-2019/photo3.jpg imgaes/noel-2018/cadeau-papa.png imgaes/noel-2018/cadeau-sarah.png imgaes/noel-2018/cadeau-jonathan.png
```

Pour commencer on constate que le dossier `imgaes` porte un nom un peu bizarre, sans doute une faute de frappe. Renommez-le correctement.

```sh
$ mv imgaes images
```

On souhaite développer toutes les photos de Noël 2018, ainsi que la "photo 1" des vacances de 2019. Pour cela on veut *les copier* dans le dossier `images/a-developper/` que l'on va envoyer au développeur de photos.

Il y a plusieurs manières de procéder, mais il est possible de tout faire en une seule commande.

La méthode la plus rapide est d'indiquer plusieurs paramètres à la commande `cp` : le premier pour récupérer toutes les photos de Noël, le second pour prendre la "photo 1" des vacances, le dernier pour spécifier la destination des 2 premiers paramètres.

```sh
$ cp images/noel-2018/*.png images/vacances-2019/photo1.jpg images/a-developper/
```

Mais il était aussi possible de déplacer les fichiers un par un, c'est beaucoup plus fastidieux et c'est là qu'on voit la puissance de la ligne de commande (et des métacaractères).

