# Quiz - Code

Kyâne Pichou

De quelle(s) manière(s) est-il possible de récupérer le résultat d'une commande pour l'utiliser dans une autre commande ?

`commande1 | commande2`

`commande1 >> commande2`

`res=$(commande1)`

`commande2 $res`

`commande1 < commande2`

Que fait la commande suivante `cat monfichier | wc -m`

Elle retourne le nombre de lignes, de mots et la taille du fichier `monfichier`

Elle affiche le contenu du fichier `monfichier`

Elle retourne le nombre de lettre dans le fichier `monfichier`

