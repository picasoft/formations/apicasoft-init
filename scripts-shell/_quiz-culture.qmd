# Quiz - Culture

Kyâne Pichou, Romain de Laage

Dans un script les boucles permettent

de répéter une action tant qu'une condition reste vraie

de renvoyer le résultat d'une commande à une autre commande

de répéter une même action sur tout les éléments d'une liste

de lier plusieurs variables entre elles

Quel est la bonne syntaxe si l'on souhaite lancer un script nommé "calcul.sh" avec pour paramètres `4` et `12`

PARAMS=4,12 ./calcul.sh

./calcul.sh --params 4 12

./calcul.sh 4 12

./calcul.sh $1=4 $2=12

Comment est-il possible de faire référence à une variable `mavar` en Bash ?

`!mavar`

`mavar`

`$mavar`

`^mavar`

`(mavar)`

Une fonction

est utilisée par le script pour calculer l'image `y` d'un point `x` à l'aide d'une expression mathématique.

permet de factoriser des instructions ou des comportements qui reviennent souvent dans le programme.

peut prendre des paramètres.

doit prendre des paramètres.

