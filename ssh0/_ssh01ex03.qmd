# Échanger des fichiers avec un serveur

Pour réaliser cet exercice vous devez être connecté à votre VPS.

On appellera `sara` le `user` connecté.

Créez un dossier `shared` dans le répertoire `/home/sara`.

`sara` doit être un `user` autorisé à se connecté en SSH au serveur.

Copiez un fichier contenant le nom de votre artiste préféré depuis votre PC vers votre VPS (dans le dossier `shared`) : `rsync -v tmp.txt sara@51.15.235.148:/sara/shared`

Créer un dossier partagé avec SFTP et l'explorateur de fichier : `sftp://sara@51.15.235.148/home/sara/shared`.

Ajoutez une liste d'œuvres de votre artiste préféré.

Créer un espace d'échange avec SFTP et Filezilla

- installer Filezilla
- configurer une connexion SFTP vers votre VPS
- déposer une image de votre artiste préféré dans `shared`

Copiez le contenu de votre dossier `shared` dans un dossier accessible à un serveur web sur votre VPS.

