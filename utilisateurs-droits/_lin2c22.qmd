# Installation d'applications sous Debian et Ubuntu (apt-get)

# Installation d'applications

Pour installer une application exécuter la commande suivante en tant que `root` ou via `sudo` :

`sudo apt-get install `

# Installer virtualbox, lftp, cups-pdf, pdftk, dia, gimp et git

```sh
sudo apt-get install virtualbox lftp cups-pdf pdftk dia gimp git
```

# Maintenir le système à jour

Pour mettre le système à jour, exécuter les deux commandes suivantes en tant que `root` ou via `sudo` :

1. `sudo apt-get update`
2. `sudo apt-get upgrade`

