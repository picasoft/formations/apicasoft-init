# Appliquer la notion

Kyâne Pichou

Je viens d'ouvrir un terminal sur une machine, mais je ne sais pas quel est l'utilisateur avec lequel je suis connecté, ni les groupes auxquels il appartient. Avec quelle commande est-il possible d'obtenir l'information ?

On utilise la commande `id`.

# Interprétation

Le retour de la commande précédente est le suivant :

```sh
uid=700(kyane) gid=700(kyane) groups=700(kyane),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),109(netdev),998(docker)
```

Quel est le nom et l'UID de l'utilisateur ?

L'utilisateur est `kyane`, avec pour UID `700`.

Quel est le GID du groupe nommé `docker` ?

Son GID est `998`.

